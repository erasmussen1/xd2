
#include "ProjectVersion.h"

#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <iostream>
#include <format>
#include <filesystem>

#include <getopt.h>

constexpr std::string VERSION{PROJECT_VERSION};


struct Options {
    Options() = default;
    ~Options() = default;

    bool show{false};
    bool decimal{false};
    bool useCppVector{false};
    std::string variableType{"uint8_t"};
    std::string variableName{"xd2Data"};
    std::string inputFilename{};
    std::string outputFilename{};
    const char* inFilename = nullptr;
    const char* outFilename = nullptr;

    auto parseCmdline(int argc, char** argv) -> int;
    auto printOptions() -> void;
    auto printVersion() -> void;
    auto printUsage(const char* programName) -> void;
};

auto Options::printVersion() -> void {
    version();
}

auto Options::printOptions() -> void {
    if (!inputFilename.empty()) {
        std::cerr << std::format("In filename: {}\n", inputFilename);
    }

    if (!outputFilename.empty()) {
        std::cerr << std::format("Out filename: {}\n", outputFilename);
    }
}

auto Options::printUsage(const char* programName) -> void {

    std::cerr << std::format("{} -- Extended dump.\n", programName);

    std::cerr << R"(
Options:
    -t typename   Use type (default is unsigned char) or uint8_t
    -n name       Variable name (default is xd2Data)
    -d            Output values in decimal (default is hex)
    -l            Output as C++ vector (default is C style array)
    -i            Input filename
    -o            Output filename
    -h            Print this message

)";
}

auto Options::parseCmdline(int argc, char** argv) -> int {
    int opt = 0;

    while ((opt = getopt(argc, argv, "n:p:t:dli:o:vuh")) != -1) {
        switch (opt) {
            case 'n':
                variableName = optarg;
                break;
            case 'p':
                // opts->pid = atoi(optarg);
                break;
            case 't':
                variableType = optarg;
                break;
            case 'd':
                decimal = true;
                break;
            case 'l':
                useCppVector = true;
                break;
            case 'i':
                inputFilename = optarg;
                break;
            case 'o':
                outputFilename = optarg;
                break;
            case 'v':
                printVersion();
                return 1;
            case 'u':
            case 'h':
            default:
                printUsage(argv[0]);
                return 1;
        }
    }

    return 0;
}

auto readFile(const std::string& inputFilename) -> std::vector<char> {

    std::ifstream ifs{inputFilename, std::ios::binary};
    if (!ifs.good()) {
        std::cerr << "Unable to open file: " << inputFilename << "\n";
        exit(1);
    }

    auto length{std::filesystem::file_size(inputFilename)};

    std::vector<char> result(length);

    ifs.read(result.data(), static_cast<long>(length));

    return result;
}

auto generateOutput(const std::vector<char>& fileData,  //
                    const std::string& variableName,    //
                    const std::string& variableType,    //
                    const bool dec,                     //
                    const bool useCppVector,            //
                    std::string& str                    //
                    ) -> int                            //
{
    if (fileData.empty()) {
        return 1;
    }

    auto formatChar = [dec](const auto ch) -> std::string {
        if (dec) {
            return std::format("{:<3d}", (unsigned char)ch);
        }

        return std::format("0x{:02X}", (unsigned char)ch);
    };

    std::stringstream ss;

    if (useCppVector) {
        ss << std::format("\n{:4s}std::vector<{:s}> {:s}{{", " ", variableType, variableName);
    } else {
        ss << std::format("\n{:4s}{:s} {:s}[] = {{", " ", variableType, variableName);
    }

    int i = 0;
    std::for_each(fileData.begin(),    //
                  fileData.end() - 1,  //
                  [&i, &ss, formatChar](const auto ch) {
                      if ((i % 12) == 0) {
                          ss << std::format("\n{:8s}", " ");
                      }

                      ss << formatChar(ch) << ", ";
                      i++;
                  });

    if (i == 0) {
        ss << std::format("\n{:8s}", " ");
    }

    ss << formatChar(fileData.back()) << "\n";
    ss << std::format("{:4s}}};\n\n", " ");

    str = ss.str();
    return 0;
}

int xd2_DumpAsCDataDeclaration(const Options& opts) {

    auto dataFromFile = readFile(opts.inputFilename);

    std::string str{};

    generateOutput(dataFromFile,       //
                   opts.variableName,  //
                   opts.variableType,  //
                   opts.decimal,       //
                   opts.useCppVector,
                   str  //
    );

    if (str.empty()) {
        return 1;
    }

    if (opts.outputFilename.empty()) {
        std::cout << str << "\n";
    }

    return 0;
}

#ifndef BUILD_FOR_UNITTEST
int main(int argc, char** argv) {
    Options opts{};

    int rc = opts.parseCmdline(argc, argv);
    if (rc) {
        return 1;
    }

    if (opts.show) {
        opts.printOptions();
        return 0;
    }

    return xd2_DumpAsCDataDeclaration(opts);
}
#endif

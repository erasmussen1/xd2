cmake_minimum_required(VERSION 3.5)

project(XD2_C_Project)

set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 23)

set(CMAKE_EXPORT_COMPILE_COMMANDS 1)

option(RELEASE_BUILD "Release build " OFF)
option(BUILD_UNITTEST "Build unittests " ON)
option(ENABLE_CCACHE "Enable ccache" OFF)
option(BUILD_SHARED  "Build as shared library" ON)
option(ENABLE_SANITIZER "Enable address sanitizer" OFF)
option(BUILD_FUZZER "Build fuzzer" OFF)
option(BUILD_BENCHMARK "Build benchmarking test" OFF)

if (BUILD_UNITTEST)
  enable_testing()
endif()


add_subdirectory(main)

if (BUILD_UNITTEST)
  add_subdirectory(test)
endif()


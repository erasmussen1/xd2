
#include <string>
#include <fstream>
#include <iostream>

#include <cstdlib>

#include <gtest/gtest.h>


extern auto generateOutput(const std::vector<char>& fileData,  //
                           const std::string& variableName,    //
                           const std::string& variableType,    //
                           const bool dec,                     //
                           const bool useCppVector,            //
                           std::string& str                    //
                           ) -> int;


struct Xd2Fixture : public testing::Test {
    Xd2Fixture() = default;
    ~Xd2Fixture() = default;

    void SetUp() {
    }

    void TearDown() {
    }

    std::vector<char> fileData;
    std::string variableName{"xd2Data"};
    std::string variableType{"unsigned char"};
    bool dec{false};
    bool useCppVector{true};
    std::string str;
    int rc = 0;
};

TEST_F(Xd2Fixture, dataWith20chars) {
    for (int i = 0; i < 20; i++)
        fileData.push_back(i + 1);
    fileData.push_back('*');

    rc = generateOutput(fileData, variableName, variableType, dec, useCppVector, str);
    EXPECT_EQ(rc, 0) << str << "\n";
}

TEST_F(Xd2Fixture, dataWith2chars) {
    fileData.push_back(31);
    fileData.push_back('*');

    rc = generateOutput(fileData, variableName, variableType, dec, useCppVector, str);
    EXPECT_EQ(rc, 0) << str << "\n";
}

TEST_F(Xd2Fixture, dataWith1char) {
    fileData.push_back('*');
    rc = generateOutput(fileData, variableName, variableType, dec, useCppVector, str);
    EXPECT_EQ(rc, 0) << str << "\n";
}



